require 'test_helper'

class TestController < ActionController::Base
  include Common::Application::Paths

  def test_page_path(*args)
    page_path *args
  end

end


class PathsTest < ActiveSupport::TestCase

  def test_inheritance
    assert TestController.new.is_a?(Common::Application::Paths)
  end

  def test_page_path
    page = pages(:committee_page)
    assert_equal '/rainbow+the-warm-colors/committee_page',
      TestController.new.test_page_path(page)
  end

  def test_page_path_with_action
    page = pages(:committee_page)
    assert_equal '/rainbow+the-warm-colors/committee_page/edit',
      TestController.new.test_page_path(page, action: 'edit')
  end

  def test_page_path_with_anchor
    page = pages(:committee_page)
    assert_equal '/rainbow+the-warm-colors/committee_page#one',
      TestController.new.test_page_path(page, anchor: 'one')
  end

  def test_page_path_with_complex_anchor
    page = pages(:committee_page)
    assert_equal '/rainbow+the-warm-colors/committee_page#%C3%B6ne%23tw%C3%BC',
      TestController.new.test_page_path(page, anchor: 'öne#twü')
  end

  def test_page_path_with_complex_query
    page = pages(:committee_page)
    assert_equal '/rainbow+the-warm-colors/committee_page?%C3%B6ne=tw%C3%BC',
      TestController.new.test_page_path(page, {'öne' => 'twü'})
  end

end
